<?php
	namespace Datast\ConfiguracionesBundle\Models;

	class VariablesGlobales
	{
		const ACTIVO = 'ACTIVO';
		const PASIVO = 'PASIVO';
		const CAPITAL = 'CAPITAL';
		const INGRESO = 'INGRESO';
		const COSTO = 'COSTO';
		const GASTO = 'GASTO';
		const OTROS_INGRESOS = 'OTROS INGRESOS';
		const OTROS_EGRESOS = 'OTROS EGRESOS';

		const BALANCE = 'BALANCE';
		const RESULTADO = 'RESULTADO';
		const DEUDOR = 'DEUDOR';
		const ACREEDOR = 'ACREEDOR';
	}