<?php

namespace Datast\ConfiguracionesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class NomenclaturaPeriodoFiscalType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cuenta')
            ->add('nombreCuenta',null,[
                'label' => 'Nombre de la cuenta',
            ])
        ;
            // ->add('estado', CheckboxType::class, [
            //     'label' => 'Estado (Activo o Inactivo)',
            //     'attr' => [
            //         'class' => 'md-checkbox'
            //     ]
            // ]);
        $builder
            ->add('btnCrear', SubmitType::class, [
                'label' => 'Guardar',
                'attr' => ['class' => 'btn btn-primary']
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Datast\ConfiguracionesBundle\Entity\NomenclaturaPeriodoFiscal'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'datast_configuracionesbundle_nomenclaturaperiodofiscal';
    }


}
