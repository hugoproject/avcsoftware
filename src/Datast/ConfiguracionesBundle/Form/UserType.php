<?php

namespace Datast\ConfiguracionesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null, [
                'label' => 'Nombre'
            ])
            ->add('username', null, [
                'label' => 'Código de usuario', 
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'options' => ['translation_domain' => 'FOSUserBundle'],
                'first_options' => ['label' => 'form.password'],
                'second_options' => ['label' => 'form.password_confirmation'],
                'invalid_message' => 'fos_user.password.mismatch',
            ])
            ->add('email', null, [
                'label' => 'form.email', 
                'translation_domain' => 'FOSUserBundle'
            ])
            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'choices' => [
                    'ROLE_SUPER_ADMIN' => 'Administrador',
                ],
                'attr' => [ 'class' => 'chosen-select' ]
            ])
            ->add('dpi', null, [
                'label' => 'DPI'
            ])
            ->add('telefono', null, [
                'label' => 'Teléfono'
            ])
            ->add('direccion', null, [
                'label' => 'Dirección'
            ])
            ->add('enabled', CheckboxType::class, [
                'label' => 'Activo',
                'attr' => [ 'align_with_widget' => true ]
            ]);

        $builder
            ->add('btnCrear', SubmitType::class, [
                'label' => 'Guardar',
                'attr' => ['class' => 'btn btn-primary']
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Datast\ConfiguracionesBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'datast_configuracionesbundle_user';
    }


}
