<?php

namespace Datast\ConfiguracionesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ConfiguracionNomenclaturaContableType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('activo')
            ->add('pasivo')
            ->add('capital')
            ->add('ingreso')
            ->add('costo')
            ->add('gasto')
            ->add('otrosIngresos')
            ->add('otrosEgresos')
        ;

        $builder
            ->add('btnCrear', SubmitType::class, [
                'label' => 'Guardar',
                'attr' => ['class' => 'btn btn-primary']
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Datast\ConfiguracionesBundle\Entity\ConfiguracionNomenclaturaContable'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'datast_configuracionesbundle_configuracionnomenclaturacontable';
    }


}
