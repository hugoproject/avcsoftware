<?php

namespace Datast\ConfiguracionesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EmpresaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('razonsocial', null, [
                'label' => 'Razonsocial',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('nombreComercial', null, [
                'label' => 'Nombre comercial',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('nit', null, [
                'label' => 'NIT',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('direccion', null, [
                'label' => 'Dirección',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('telefono', null, [
                'label' => 'Teléfono',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('moneda', EntityType::class, [
                'class' => 'DatastConfiguracionesBundle:Moneda',
                'choice_label' => 'nombre',
                'label' => 'Moneda',
                'attr' => ['class' => 'select2-placeholder-single col-md-3']
            ])
            ->add('monedaInternacional', EntityType::class, [
                'class' => 'DatastConfiguracionesBundle:Moneda',
                'choice_label' => 'nombre',
                'label' => 'Moneda para transacciones internacionales',
                'attr' => ['class' => 'select2-placeholder-single col-md-3']
            ])
            ->add('nombreLogo', null, [
                'label' => 'Logo',
                'required'    => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ]);

        $builder
            ->add('btnCrear', SubmitType::class, [
                'label' => 'Guardar',
                'attr' => ['class' => 'btn btn-primary']
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Datast\ConfiguracionesBundle\Entity\Empresa'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'datast_configuracionesbundle_empresa';
    }


}
