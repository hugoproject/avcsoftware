<?php

namespace Datast\ConfiguracionesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class PeriodoFiscalType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fechaInicio', DateType::class,array(
                'label'=> 'Fecha inicio',
                'widget'=> 'single_text',
                'html5' => false,
                'format'=>'dd/MM/yyyy',
                'attr' => ['class' => 'js-datepicker'],
            ))
            ->add('fechaFin', DateType::class,array(
                'label'=> 'Fecha finalización',
                'widget'=> 'single_text',
                'html5' => false,
                'format'=>'dd/MM/yyyy',
                'attr' => ['class' => 'js-datepicker'],
            ));

        $builder
            ->add('btnCrear', SubmitType::class, [
                'label' => 'Guardar y crear nueva nomenclatura',
                'attr' => ['class' => 'btn btn-primary']
            ])
            ->add('btnCopiar', SubmitType::class, [
                'label' => 'Guardar y copiar nomenclatura existente',
                'attr' => ['class' => 'btn yellow-mint']
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Datast\ConfiguracionesBundle\Entity\PeriodoFiscal'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'datast_configuracionesbundle_periodofiscal';
    }


}
