<?php

namespace Datast\ConfiguracionesBundle\Controller;

use Datast\ConfiguracionesBundle\Entity\Moneda;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Moneda controller.
 *
 * @Route("config/moneda")
 */
class MonedaController extends Controller
{
    /**
     * Lists all moneda entities.
     *
     * @Route("/", name="config_moneda_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $monedas = $em->getRepository('DatastConfiguracionesBundle:Moneda')->findAll();

        return $this->render('moneda/index.html.twig', array(
            'monedas' => $monedas,
        ));
    }

    /**
     * Creates a new moneda entity.
     *
     * @Route("/new", name="config_moneda_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $moneda = new Moneda();
        $form = $this->createForm('Datast\ConfiguracionesBundle\Form\MonedaType', $moneda);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($moneda);
            $em->flush($moneda);

            return $this->redirectToRoute('config_moneda_index');
        }

        return $this->render('moneda/new.html.twig', array(
            'moneda' => $moneda,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a moneda entity.
     *
     * @Route("/{id}", name="config_moneda_show")
     * @Method("GET")
     */
    public function showAction(Moneda $moneda)
    {
        $deleteForm = $this->createDeleteForm($moneda);

        return $this->render('moneda/show.html.twig', array(
            'moneda' => $moneda,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing moneda entity.
     *
     * @Route("/{id}/edit", name="config_moneda_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Moneda $moneda)
    {
        $deleteForm = $this->createDeleteForm($moneda);
        $editForm = $this->createForm('Datast\ConfiguracionesBundle\Form\MonedaType', $moneda);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('config_moneda_index');
        }

        return $this->render('moneda/edit.html.twig', array(
            'moneda' => $moneda,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a moneda entity.
     *
     * @Route("/{id}", name="config_moneda_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Moneda $moneda)
    {
        $form = $this->createDeleteForm($moneda);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($moneda);
            $em->flush($moneda);
        }

        return $this->redirectToRoute('config_moneda_index');
    }

    /**
     * Creates a form to delete a moneda entity.
     *
     * @param Moneda $moneda The moneda entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Moneda $moneda)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('config_moneda_delete', array('id' => $moneda->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
