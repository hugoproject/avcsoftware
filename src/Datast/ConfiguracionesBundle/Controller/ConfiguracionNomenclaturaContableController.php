<?php

namespace Datast\ConfiguracionesBundle\Controller;

use Datast\ConfiguracionesBundle\Entity\ConfiguracionNomenclaturaContable;
use Datast\ConfiguracionesBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Configuracionnomenclaturacontable controller.
 *
 * @Route("config/configuracionnomenclaturacontable")
 */
class ConfiguracionNomenclaturaContableController extends BaseController
{
    /**
     * Lists all configuracionNomenclaturaContable entities.
     *
     * @Route("/", name="config_configuracionnomenclaturacontable_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $repository = $this->getEM()
            ->getRepository('DatastConfiguracionesBundle:ConfiguracionNomenclaturaContable');

        $query = $repository
            ->createQueryBuilder('nc');

        $nomenclaturaContable = $query
            ->getQuery()
            ->getOneOrNullResult();

        if ($nomenclaturaContable) {
            return $this->redirectToRoute(
                'config_configuracionnomenclaturacontable_edit',
                ['id' => $nomenclaturaContable->getId()]
            );
        } else {
            return $this->redirectToRoute('config_configuracionnomenclaturacontable_new');
        }
    }

    /**
     * Creates a new configuracionNomenclaturaContable entity.
     *
     * @Route("/new", name="config_configuracionnomenclaturacontable_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $configuracionNomenclaturaContable = new Configuracionnomenclaturacontable();

        $form = $this
            ->createForm(
                'Datast\ConfiguracionesBundle\Form\ConfiguracionNomenclaturaContableType',
                $configuracionNomenclaturaContable
            );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getEM();
            $em->persist($configuracionNomenclaturaContable);
            $em->flush();

            $this->addFlash(
                'notice',
                'Los cambios han sido guardados!'
            );

            return $this->redirectToRoute('config_configuracionnomenclaturacontable_edit', [
                'id' => $configuracionNomenclaturaContable->getId()
            ]);
        }

        return $this->render('configuracionnomenclaturacontable/new.html.twig', array(
            'configuracionNomenclaturaContable' => $configuracionNomenclaturaContable,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing configuracionNomenclaturaContable entity.
     *
     * @Route("/{id}/edit", name="config_configuracionnomenclaturacontable_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ConfiguracionNomenclaturaContable $configuracionNomenclaturaContable)
    {
        $deleteForm = $this->createDeleteForm($configuracionNomenclaturaContable);
        $editForm = $this->createForm(
            'Datast\ConfiguracionesBundle\Form\ConfiguracionNomenclaturaContableType',
            $configuracionNomenclaturaContable);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getEM()->flush();

            $this->addFlash(
                'notice',
                'Los cambios han sido guardados!'
            );

            return $this->redirectToRoute('config_configuracionnomenclaturacontable_edit', [
                'id' => $configuracionNomenclaturaContable->getId()
            ]);
        }

        return $this->render('configuracionnomenclaturacontable/edit.html.twig', array(
            'configuracionNomenclaturaContable' => $configuracionNomenclaturaContable,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a configuracionNomenclaturaContable entity.
     *
     * @Route("/{id}", name="config_configuracionnomenclaturacontable_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ConfiguracionNomenclaturaContable $configuracionNomenclaturaContable)
    {
        $form = $this
            ->createDeleteForm($configuracionNomenclaturaContable);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getEM();
            $em->remove($configuracionNomenclaturaContable);
            $em->flush();
        }

        return $this->redirectToRoute('config_configuracionnomenclaturacontable_index');
    }

    /**
     * Creates a form to delete a configuracionNomenclaturaContable entity.
     *
     * @param ConfiguracionNomenclaturaContable $configuracionNomenclaturaContable The configuracionNomenclaturaContable entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ConfiguracionNomenclaturaContable $configuracionNomenclaturaContable)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('config_configuracionnomenclaturacontable_delete', array('id' => $configuracionNomenclaturaContable->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
