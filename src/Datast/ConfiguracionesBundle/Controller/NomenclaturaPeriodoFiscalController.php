<?php

namespace Datast\ConfiguracionesBundle\Controller;

use Datast\ConfiguracionesBundle\Entity\NomenclaturaPeriodoFiscal;
use Datast\ConfiguracionesBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Nomenclaturaperiodofiscal controller.
 *
 * @Route("empresa/{periodo_fiscal_id}/nomenclaturaperiodofiscal")
 */
class NomenclaturaPeriodoFiscalController extends BaseController
{
    /**
     * Lists all nomenclaturaPeriodoFiscal entities.
     *
     * @Route("/", name="empresa_nomenclaturaperiodofiscal_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, $periodo_fiscal_id)
    {
        $em = $this->getEM();

        $nomenclaturaPeriodoFiscals = $em
            ->getRepository('DatastConfiguracionesBundle:NomenclaturaPeriodoFiscal')
            ->findBy(
                ['periodoFiscal' => $periodo_fiscal_id],
                ['cuenta' => 'ASC']
            );

        return $this->render('nomenclaturaperiodofiscal/index.html.twig', [
            'nomenclaturaPeriodoFiscals' => $nomenclaturaPeriodoFiscals,
            'periodoFiscalId' => $periodo_fiscal_id
        ]);
    }

    /**
     * Creates a new nomenclaturaPeriodoFiscal entity.
     *
     * @Route("/{nodo_padre_id}/new", name="empresa_nomenclaturaperiodofiscal_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $periodo_fiscal_id, $nodo_padre_id)
    {
        $em = $this->getEM();
        $periodoFiscal = $em
            ->getRepository('DatastConfiguracionesBundle:PeriodoFiscal')
            ->find($periodo_fiscal_id);

        $nomenclaturaPeriodoFiscalPorNivel = $this->getNomenclaturaPeriodoFiscalPorNivel($nodo_padre_id);

        $nomenclaturaNodoPadre = $em
            ->getRepository('DatastConfiguracionesBundle:NomenclaturaPeriodoFiscal')
            ->find($nodo_padre_id);

        $nomenclaturaPeriodoFiscal = new Nomenclaturaperiodofiscal();
        $nomenclaturaPeriodoFiscal->setPeriodoFiscal($periodoFiscal);
        $nomenclaturaPeriodoFiscal->setNodoPadre($nomenclaturaNodoPadre);
        $nomenclaturaPeriodoFiscal->setNivel($nomenclaturaNodoPadre->getNivel() + 1);
        $nomenclaturaPeriodoFiscal->setAltura($nomenclaturaPeriodoFiscal->getNivel() + 1);
        $nomenclaturaPeriodoFiscal->setNaturaleza($nomenclaturaNodoPadre->getNaturaleza());
        $nomenclaturaPeriodoFiscal->setTipoDeCuenta($nomenclaturaNodoPadre->getTipoDeCuenta());
        $nomenclaturaNodoPadre->setAltura($nomenclaturaPeriodoFiscal->getAltura());


        $form = $this->createForm(
            'Datast\ConfiguracionesBundle\Form\NomenclaturaPeriodoFiscalType',
            $nomenclaturaPeriodoFiscal
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($nomenclaturaNodoPadre);
            $em->persist($nomenclaturaPeriodoFiscal);
            $em->flush();

            $this->addFlash(
                'notice',
                'La cuenta a sido creada!'
            );

            return $this->redirectToRoute(
                'empresa_nomenclaturaperiodofiscal_new', [
                    'nodo_padre_id' => $nodo_padre_id,
                    'periodo_fiscal_id' => $periodo_fiscal_id
                ]);
        }

        return $this->render('nomenclaturaperiodofiscal/new.html.twig', [
            'nomenclaturaPeriodoFiscal' => $nomenclaturaPeriodoFiscal,
            'resumenCuentas' => $nomenclaturaPeriodoFiscalPorNivel,
            'form' => $form->createView(),
        ]);
    }

    private function getNomenclaturaPeriodoFiscalPorNivel($nodoPadreId)
    {
        $nomenclatura = new ArrayCollection();
        $em = $this->getEM();
        $nomenclaturaPeriodoFiscal = $em
            ->getRepository('DatastConfiguracionesBundle:NomenclaturaPeriodoFiscal')
            ->findBy(
                ['nodoPadre' => $nodoPadreId],
                ['cuenta' => 'DESC']
            );

        foreach ($nomenclaturaPeriodoFiscal as $value) {
            $nomenclatura->add($value);
        }

        $this->getNodoPadre($nodoPadreId, $nomenclatura);

        return $nomenclatura;
    }

    private function getNodoPadre($nodoPadreId, $nomenclatura)
    {
        $em = $this->getEM();
        $nomenclaturaPeriodoFiscal = $em
            ->getRepository('DatastConfiguracionesBundle:NomenclaturaPeriodoFiscal')
            ->find($nodoPadreId);

        $nomenclatura->add($nomenclaturaPeriodoFiscal);

        if ($nomenclaturaPeriodoFiscal->getNodoPadre()) {
            $this->getNodoPadre($nomenclaturaPeriodoFiscal->getNodoPadre()->getId(), $nomenclatura);
        }
    }

    private function getNodoRaiz($nodoPadreId)
    {
        $em = $this->getEM();
        $nomenclaturaPeriodoFiscal = $em
            ->getRepository('DatastConfiguracionesBundle:NomenclaturaPeriodoFiscal')
            ->find($nodoPadreId);

        if ($nomenclaturaPeriodoFiscal->getNodoPadre()) {
            $this->getNodoRaiz($nomenclaturaPeriodoFiscal->getNodoPadre()->getId());
        }

        return $nomenclaturaPeriodoFiscal;
    }

    private function getAltura($nodoPadreId)
    {
        $altura = 1;
        $nodoRaiz = $this->getNodoRaiz($nodoPadreId);

        $altura = $this->getCalcularAltura($nodoRaiz, $altura);

        return $altura;
    }

    private function getCalcularAltura($nodoRaiz, $altura)
    {
        $em = $this->getEM();
        $nivelesNomenclaturaPeriodoFiscal = $em
            ->getRepository('DatastConfiguracionesBundle:NomenclaturaPeriodoFiscal')
            ->findBy(
                ['nodoPadre' => $nodoRaiz]
            );
        if ($nivelesNomenclaturaPeriodoFiscal) {
            foreach ($nivelesNomenclaturaPeriodoFiscal as $cuenta) {
                $this->getCalcularAltura($cuenta, $altura);
            }

            $altura++;
        }

        return $altura;
    }


    /**
     * Displays a form to edit an existing nomenclaturaPeriodoFiscal entity.
     *
     * @Route("/{id}/edit", name="empresa_nomenclaturaperiodofiscal_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, NomenclaturaPeriodoFiscal $nomenclaturaPeriodoFiscal)
    {
        $nomenclaturaPeriodoFiscalPorNivel = $this
            ->getNomenclaturaPeriodoFiscalPorNivel($nomenclaturaPeriodoFiscal->getNodoPadre());

        $deleteForm = $this->createDeleteForm($nomenclaturaPeriodoFiscal);
        $editForm = $this->createForm(
            'Datast\ConfiguracionesBundle\Form\NomenclaturaPeriodoFiscalType',
            $nomenclaturaPeriodoFiscal
        );
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $this->getEM()->flush();


            $this->addFlash(
                'notice',
                'Los cambios han sido guardados!'
            );

            return $this->redirectToRoute(
                'empresa_nomenclaturaperiodofiscal_index',[
                    'periodo_fiscal_id' => $nomenclaturaPeriodoFiscal->getPeriodoFiscal()->getId()
            ]);
        }

        return $this->render('nomenclaturaperiodofiscal/edit.html.twig', array(
            'nomenclaturaPeriodoFiscal' => $nomenclaturaPeriodoFiscal,
            'resumenCuentas' => $nomenclaturaPeriodoFiscalPorNivel,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a nomenclaturaPeriodoFiscal entity.
     *
     * @Route("/{id}", name="empresa_nomenclaturaperiodofiscal_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, NomenclaturaPeriodoFiscal $nomenclaturaPeriodoFiscal)
    {
        $em = $this->getEM();
        $form = $this->createDeleteForm($nomenclaturaPeriodoFiscal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($nomenclaturaPeriodoFiscal);
            $em->flush();

            $NomenclaturaperiodofiscalNodoPadre = $em
                ->getRepository('DatastConfiguracionesBundle:NomenclaturaPeriodoFiscal')
                ->find($nomenclaturaPeriodoFiscal->getNodoPadre());

            if (!$this->getTieneNodosHijos($NomenclaturaperiodofiscalNodoPadre)) {
                $NomenclaturaperiodofiscalNodoPadre
                    ->setAltura(
                        $NomenclaturaperiodofiscalNodoPadre->getNivel() + 1
                    );
                $em->persist($NomenclaturaperiodofiscalNodoPadre);
                $em->flush();
            }
        }

        return $this->redirectToRoute(
            'empresa_nomenclaturaperiodofiscal_index',[
                'periodo_fiscal_id' => $nomenclaturaPeriodoFiscal->getPeriodoFiscal()->getId()
        ]);
    }

    private function getTieneNodosHijos($nodo)
    {
        $em = $this->getEM();
        $nivelesNomenclaturaPeriodoFiscal = $em
            ->getRepository('DatastConfiguracionesBundle:NomenclaturaPeriodoFiscal')
            ->findBy(
                ['nodoPadre' => $nodo]
            );
        if ($nivelesNomenclaturaPeriodoFiscal) {
            return true;
        }

        return false;
    }

    /**
     * Creates a form to delete a nomenclaturaPeriodoFiscal entity.
     *
     * @param NomenclaturaPeriodoFiscal $nomenclaturaPeriodoFiscal The nomenclaturaPeriodoFiscal entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(NomenclaturaPeriodoFiscal $nomenclaturaPeriodoFiscal)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('empresa_nomenclaturaperiodofiscal_delete', [
                'id' => $nomenclaturaPeriodoFiscal->getId(),
                'periodo_fiscal_id' => $nomenclaturaPeriodoFiscal->getPeriodoFiscal()->getId()
            ]))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
