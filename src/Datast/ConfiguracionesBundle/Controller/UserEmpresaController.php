<?php

namespace Datast\ConfiguracionesBundle\Controller;

use Datast\ConfiguracionesBundle\Entity\UserEmpresa;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Datast\ConfiguracionesBundle\Form\UserEmpresaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * UserEmpresa controller.
 *
 * @Route("config/user/{user_id}/empresas")
 */
class UserEmpresaController extends Controller
{
    /**
     * Lists all  entities.
     *
     * @Route("/", name="config_user_empresa_index")
     * @Template()
     * @Method("GET")
     */
    public function indexAction(Request $request, $user_id)
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em
            ->getRepository('DatastConfiguracionesBundle:User')
            ->find($user_id);

        return [
            'empresas' => $users->getEmpresas(),
            'user_id' => $user_id
        ];
    }

    /**
     * Creates a new user_empresa entity.
     *
     * @Route("/new", name="config_user_empresa_new")
     * @Template()
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $user_id)
    {
        $userManager = $this->container->get('fos_user.user_manager');
        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository("DatastConfiguracionesBundle:User")
            ->find($user_id);

        $form = $this->createFormBuilder()
            ->add('empresa', EntityType::class, [
                'class' => 'DatastConfiguracionesBundle:Empresa',
                'choice_label' => 'razonsocial',
                'label' => 'Empresa',
                'attr' => ['class' => 'select2-placeholder-single']
            ])
            ->add('btnCrear', SubmitType::class, [
                'label' => 'Asociar a usuario',
                'attr' => ['class' => 'btn btn-primary']
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->addEmpresa(
                $form->getData()['empresa']
            );

            $userManager->updateUser($user);

            return $this->redirectToRoute(
                'config_user_empresa_index', 
                ['user_id' => $user->getId()]
            );
        }

        return [
            'form' => $form->createView(),
            'user' => $user
        ];
    }

    /**
     * Deletes a pai entity.
     *
     * @Route("/{empresa_id}", name="config_user_empresa_delete")
     */
    public function deleteAction(
        Request $request, $user_id, $empresa_id
    )
    {
        $em = $this->getDoctrine()->getEntityManager();
        $userManager = $this->container->get('fos_user.user_manager');
        
        $user = $em->getRepository("DatastConfiguracionesBundle:User")
            ->find($user_id);
        $empresa = $em->getRepository("DatastConfiguracionesBundle:Empresa")
            ->find($empresa_id);

        $user->removeEmpresa(
            $empresa
        );

        $userManager->updateUser($user);

        return $this->redirectToRoute("config_user_empresa_index",
            ['user_id' => $user->getId()]
        );
    }
}
