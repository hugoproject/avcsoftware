<?php

namespace Datast\ConfiguracionesBundle\Controller;

use Datast\ConfiguracionesBundle\Entity\TipoDocumento;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Datast\ConfiguracionesBundle\Controller\BaseController;

/**
 * Tipodocumento controller.
 *
 * @Route("config/tipodocumento")
 */
class TipoDocumentoController extends BaseController
{
    /**
     * Lists all tipoDocumento entities.
     *
     * @Route("/", name="config_tipodocumento_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getEM();

        $tipoDocumentos = $em
            ->getRepository('DatastConfiguracionesBundle:TipoDocumento')
            ->findAll();

        return $this->render('tipodocumento/index.html.twig', [
            'tipoDocumentos' => $tipoDocumentos,
        ]);
    }

    /**
     * Creates a new tipoDocumento entity.
     *
     * @Route("/new", name="config_tipodocumento_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $tipoDocumento = new Tipodocumento();
        $form = $this->createForm(
            'Datast\ConfiguracionesBundle\Form\TipoDocumentoType',
            $tipoDocumento
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getEM();
            $em->persist($tipoDocumento);
            $em->flush();

            return $this->redirectToRoute('config_tipodocumento_show', [
                'id' => $tipoDocumento->getId()
            ]);
        }

        return $this->render('tipodocumento/new.html.twig', [
            'tipoDocumento' => $tipoDocumento,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a tipoDocumento entity.
     *
     * @Route("/{id}", name="config_tipodocumento_show")
     * @Method("GET")
     */
    public function showAction(TipoDocumento $tipoDocumento)
    {
        $deleteForm = $this->createDeleteForm($tipoDocumento);

        return $this->render('tipodocumento/show.html.twig', array(
            'tipoDocumento' => $tipoDocumento,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tipoDocumento entity.
     *
     * @Route("/{id}/edit", name="config_tipodocumento_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TipoDocumento $tipoDocumento)
    {
        $deleteForm = $this->createDeleteForm($tipoDocumento);
        $editForm = $this->createForm('Datast\ConfiguracionesBundle\Form\TipoDocumentoType', $tipoDocumento);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('config_tipodocumento_edit', array('id' => $tipoDocumento->getId()));
        }

        return $this->render('tipodocumento/edit.html.twig', array(
            'tipoDocumento' => $tipoDocumento,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tipoDocumento entity.
     *
     * @Route("/{id}", name="config_tipodocumento_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TipoDocumento $tipoDocumento)
    {
        $form = $this->createDeleteForm($tipoDocumento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tipoDocumento);
            $em->flush();
        }

        return $this->redirectToRoute('config_tipodocumento_index');
    }

    /**
     * Creates a form to delete a tipoDocumento entity.
     *
     * @param TipoDocumento $tipoDocumento The tipoDocumento entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TipoDocumento $tipoDocumento)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('config_tipodocumento_delete', array('id' => $tipoDocumento->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
