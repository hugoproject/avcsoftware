<?php

namespace Datast\ConfiguracionesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class BaseController extends Controller
{
    protected $session;

    protected $em;

    protected $empresa;

    public function getSession()
    {
        $this->session = $this->container->get('session');

        return $this->session;
    }

    public function getEmpresa()
    {
        $this->empresa = $this->getSession()->get('empresa');

        return $this->empresa;
    }

    public function getEM()
    {
        $this->em = $this->getDoctrine()->getManager();

        return $this->em;
    }

}
