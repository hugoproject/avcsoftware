<?php

namespace Datast\ConfiguracionesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FormType;

/**
 * HomePage controller.
 *
 */
class HomePageController extends Controller
{
    /**
     *
     * @Route("/")
     * @Method({"GET", "POST"})
     * @Template("DatastConfiguracionesBundle:homePage:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $usuario = $this->getUser();

        $form = $this->createFormBuilder()
            ->add('empresa', EntityType::class, [
                'class' => 'DatastConfiguracionesBundle:Empresa',
                'choice_label' => 'razonsocial',
                'label' => 'Empresa',
                'choices' => $usuario->getEmpresas(),
                'attr' => ['class' => 'select2-placeholder-single'],
            ])
            ->getForm();

        $form->handleRequest($request);

        if (!$form->isValid()) {
            return [
                'form' => $form->createView()
            ];
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $empresaSeleccionada = $form->getData()['empresa'];

        $this->container->get('session')
            ->set('empresa', $empresaSeleccionada);

        return $this->redirectToRoute('datast_configuraciones_homepage_dashboard');
    }

    /**
     *
     * @Route("/dashboard", name="datast_configuraciones_homepage_dashboard")
     * @Method("GET")
     * @Template("DatastConfiguracionesBundle:homePage:dashboard.html.twig")
     */
    public function dashboardAction(Request $request)
    {
        return;
    }
}
