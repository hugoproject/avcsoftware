<?php

namespace Datast\ConfiguracionesBundle\Controller;

use Datast\ConfiguracionesBundle\Entity\PeriodoFiscal;
use Datast\ConfiguracionesBundle\Entity\Empresa;
use Datast\ConfiguracionesBundle\Entity\ConfiguracionNomenclaturaContable;
use Datast\ConfiguracionesBundle\Entity\NomenclaturaPeriodoFiscal;
use Datast\ConfiguracionesBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Datast\ConfiguracionesBundle\Models\VariablesGlobales;

/**
 * Periodofiscal controller.
 *
 * @Route("empresa/periodofiscal")
 */
class PeriodoFiscalController extends BaseController
{
    /**
     * Lists all periodoFiscal entities.
     *
     * @Route("/", name="empresa_periodofiscal_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getEM();

        $periodoFiscals = $em
            ->getRepository('DatastConfiguracionesBundle:PeriodoFiscal')
            ->findAll();

        return $this->render('periodofiscal/index.html.twig', [
            'periodoFiscals' => $periodoFiscals,
        ]);
    }

    /**
     * Creates a new periodoFiscal entity.
     *
     * @Route("/new", name="empresa_periodofiscal_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $periodoFiscal = new Periodofiscal();
        $em = $this->getEM();

        $empresa = $em
            ->getRepository('DatastConfiguracionesBundle:Empresa')
            ->find($this->getEmpresa());

        $periodoFiscal->setEmpresa($empresa);

        $form = $this->createForm(
            'Datast\ConfiguracionesBundle\Form\PeriodoFiscalType',
            $periodoFiscal
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getEM();
            $em->persist($periodoFiscal);
            $em->flush();

            if ($form->get('btnCrear')->isClicked()) {
                $this->crearNuevaNomenclatura($periodoFiscal);
            }

            return $this->redirectToRoute('empresa_periodofiscal_index', [
                'id' => $periodoFiscal->getId()
            ]);
        }

        return $this->render('periodofiscal/new.html.twig', [
            'periodoFiscal' => $periodoFiscal,
            'form' => $form->createView(),
        ]);
    }

    private function crearNuevaNomenclatura($periodoFiscal) 
    {
        $ejecutarFlush = false;

        $em = $this->getEM();
        $configNomenclatura = $em
            ->getRepository('DatastConfiguracionesBundle:ConfiguracionNomenclaturaContable')
            ->createQueryBuilder('c')
            ->getQuery()
            ->getOneOrNullResult();

        if ($configNomenclatura) {
            if ($configNomenclatura->getActivo()) {
                $crearCuentaActivo = new NomenclaturaPeriodoFiscal();
                $crearCuentaActivo->setPeriodoFiscal($periodoFiscal);
                $crearCuentaActivo->setCuenta($configNomenclatura->getActivo());
                $crearCuentaActivo->setNombreCuenta(VariablesGlobales::ACTIVO);       
                // $crearCuentaActivo->setNodoPadre(null);
                $crearCuentaActivo->setNivel(0);
                $crearCuentaActivo->setAltura(1);
                $crearCuentaActivo->setNaturaleza(VariablesGlobales::DEUDOR);
                $crearCuentaActivo->setTipoDeCuenta(VariablesGlobales::BALANCE);
                $em->persist($crearCuentaActivo);
                $ejecutarFlush = true;
            }

            if ($configNomenclatura->getPasivo()) {
                $crearCuentaPasivo = new NomenclaturaPeriodoFiscal();
                $crearCuentaPasivo->setPeriodoFiscal($periodoFiscal);
                $crearCuentaPasivo->setCuenta($configNomenclatura->getPasivo());
                $crearCuentaPasivo->setNombreCuenta(VariablesGlobales::PASIVO);       
                $crearCuentaPasivo->setNivel(0);
                $crearCuentaPasivo->setAltura(1);
                $crearCuentaPasivo->setNaturaleza(VariablesGlobales::ACREEDOR);
                $crearCuentaPasivo->setTipoDeCuenta(VariablesGlobales::BALANCE);
                $em->persist($crearCuentaPasivo);
                $ejecutarFlush = true;
            }

            if ($configNomenclatura->getCapital()) {
                $crearCuentaCapital = new NomenclaturaPeriodoFiscal();
                $crearCuentaCapital->setPeriodoFiscal($periodoFiscal);
                $crearCuentaCapital->setCuenta($configNomenclatura->getCapital());
                $crearCuentaCapital->setNombreCuenta(VariablesGlobales::CAPITAL);       
                $crearCuentaCapital->setNivel(0);
                $crearCuentaCapital->setAltura(1);
                $crearCuentaCapital->setNaturaleza(VariablesGlobales::ACREEDOR);
                $crearCuentaCapital->setTipoDeCuenta(VariablesGlobales::BALANCE);
                $em->persist($crearCuentaCapital);
                $ejecutarFlush = true;
            }

            if ($configNomenclatura->getIngreso()) {
                $crearCuentaIngreso = new NomenclaturaPeriodoFiscal();
                $crearCuentaIngreso->setPeriodoFiscal($periodoFiscal);
                $crearCuentaIngreso->setCuenta($configNomenclatura->getIngreso());
                $crearCuentaIngreso->setNombreCuenta(VariablesGlobales::INGRESO);       
                $crearCuentaIngreso->setNivel(0);
                $crearCuentaIngreso->setAltura(1);
                $crearCuentaIngreso->setNaturaleza(VariablesGlobales::ACREEDOR);
                $crearCuentaIngreso->setTipoDeCuenta(VariablesGlobales::RESULTADO);
                $em->persist($crearCuentaIngreso);
                $ejecutarFlush = true;
            }

            if ($configNomenclatura->getCosto()) {
                $crearCuentaCosto = new NomenclaturaPeriodoFiscal();
                $crearCuentaCosto->setPeriodoFiscal($periodoFiscal);
                $crearCuentaCosto->setCuenta($configNomenclatura->getCosto());
                $crearCuentaCosto->setNombreCuenta(VariablesGlobales::COSTO);       
                $crearCuentaCosto->setNivel(0);
                $crearCuentaCosto->setAltura(1);
                $crearCuentaCosto->setNaturaleza(VariablesGlobales::DEUDOR);
                $crearCuentaCosto->setTipoDeCuenta(VariablesGlobales::RESULTADO);
                $em->persist($crearCuentaCosto);
                $ejecutarFlush = true;
            }

            if ($configNomenclatura->getGasto()) {
                $crearCuentaGasto = new NomenclaturaPeriodoFiscal();
                $crearCuentaGasto->setPeriodoFiscal($periodoFiscal);
                $crearCuentaGasto->setCuenta($configNomenclatura->getGasto());
                $crearCuentaGasto->setNombreCuenta(VariablesGlobales::GASTO);       
                $crearCuentaGasto->setNivel(0);
                $crearCuentaGasto->setAltura(1);
                $crearCuentaGasto->setNaturaleza(VariablesGlobales::DEUDOR);
                $crearCuentaGasto->setTipoDeCuenta(VariablesGlobales::RESULTADO);
                $em->persist($crearCuentaGasto);
                $ejecutarFlush = true;
            }

            if ($configNomenclatura->getOtrosIngresos()) {
                $crearCuentaOtrosIngresos = new NomenclaturaPeriodoFiscal();
                $crearCuentaOtrosIngresos->setPeriodoFiscal($periodoFiscal);
                $crearCuentaOtrosIngresos->setCuenta($configNomenclatura->getOtrosIngresos());
                $crearCuentaOtrosIngresos->setNombreCuenta(VariablesGlobales::OTROS_INGRESOS);       
                $crearCuentaOtrosIngresos->setNivel(0);
                $crearCuentaOtrosIngresos->setAltura(1);
                $crearCuentaOtrosIngresos->setNaturaleza(VariablesGlobales::ACREEDOR);
                $crearCuentaOtrosIngresos->setTipoDeCuenta(VariablesGlobales::RESULTADO);
                $em->persist($crearCuentaOtrosIngresos);
                $ejecutarFlush = true;
            }

            if ($configNomenclatura->getOtrosEgresos()) {
                $crearCuentaOtrosEgresos = new NomenclaturaPeriodoFiscal();
                $crearCuentaOtrosEgresos->setPeriodoFiscal($periodoFiscal);
                $crearCuentaOtrosEgresos->setCuenta($configNomenclatura->getOtrosEgresos());
                $crearCuentaOtrosEgresos->setNombreCuenta(VariablesGlobales::OTROS_EGRESOS);       
                $crearCuentaOtrosEgresos->setNivel(0);
                $crearCuentaOtrosEgresos->setAltura(1);
                $crearCuentaOtrosEgresos->setNaturaleza(VariablesGlobales::DEUDOR);
                $crearCuentaOtrosEgresos->setTipoDeCuenta(VariablesGlobales::RESULTADO);
                $em->persist($crearCuentaOtrosEgresos);
                $ejecutarFlush = true;
            }
            if ($ejecutarFlush) {
                $em->flush();
            }
        }   
    }

    /**
     * Displays a form to edit an existing periodoFiscal entity.
     *
     * @Route("/{id}/edit", name="empresa_periodofiscal_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PeriodoFiscal $periodoFiscal)
    {
        $deleteForm = $this->createDeleteForm($periodoFiscal);
        $editForm = $this->createForm(
            'Datast\ConfiguracionesBundle\Form\PeriodoFiscalType',
            $periodoFiscal
        );
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getEM()                ;

            return $this->redirectToRoute('empresa_periodofiscal_index');

        }

        return $this->render('periodofiscal/edit.html.twig', [
            'periodoFiscal' => $periodoFiscal,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Deletes a periodoFiscal entity.
     *
     * @Route("/{id}", name="empresa_periodofiscal_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, PeriodoFiscal $periodoFiscal)
    {
        $form = $this->createDeleteForm($periodoFiscal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($periodoFiscal);
            $em->flush();
        }

        return $this->redirectToRoute('empresa_periodofiscal_index');
    }

    /**
     * Creates a form to delete a periodoFiscal entity.
     *
     * @param PeriodoFiscal $periodoFiscal The periodoFiscal entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PeriodoFiscal $periodoFiscal)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl(
                'empresa_periodofiscal_delete', [
                    'id' => $periodoFiscal->getId()
            ]))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
