<?php

namespace Datast\ConfiguracionesBundle\Controller;

use Datast\ConfiguracionesBundle\Entity\CPO;
use Datast\ConfiguracionesBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Catalogocontacto controller.
 *
 * @Route("config/cpo")
 */
class CPOController extends BaseController
{
    /**
     * Lists all CPO entities.
     *
     * @Route("/", name="config_cpo_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getEM();

        $cpo = $em
            ->getRepository('DatastConfiguracionesBundle:CPO')
            ->findAll();

        return $this->render('cpo/index.html.twig', [
            'cpo' => $cpo,
        ]);
    }

    /**
     * Creates a new CPO entity.
     *
     * @Route("/new", name="config_cpo_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $cpo = new CPO();
        $form = $this->createForm(
            'Datast\ConfiguracionesBundle\Form\CPOType',
            $cpo
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cpo);
            $em->flush($cpo);

            return $this->redirectToRoute('config_cpo_new');
        }

        return $this->render('cpo/new.html.twig', [
            'cpo' => $cpo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a CPO entity.
     *
     * @Route("/{id}", name="config_cpo_show")
     * @Method("GET")
     */
    public function showAction(CPO $cpo)
    {
        $deleteForm = $this->createDeleteForm($cpo);

        return $this->render('cpo/show.html.twig', [
            'cpo' => $cpo,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing CPO entity.
     *
     * @Route("/{id}/edit", name="config_cpo_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, CPO $cpo)
    {
        $deleteForm = $this->createDeleteForm($cpo);
        $editForm = $this->createForm(
            'Datast\ConfiguracionesBundle\Form\CPOType',
            $cpo
        );
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('config_cpo_index');
        }

        return $this->render('cpo/edit.html.twig', array(
            'cpo' => $cpo,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a CPO entity.
     *
     * @Route("/{id}", name="config_cpo_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, CPO $cpo)
    {
        $form = $this->createDeleteForm($cpo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cpo);
            $em->flush($cpo);
        }

        return $this->redirectToRoute('config_cpo_index');
    }

    /**
     * Creates a form to delete a CPO entity.
     *
     * @param CPO $cpo The CPO entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CPO $cpo)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl(
                'config_cpo_delete', [
                    'id' => $cpo->getId()
                ]))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
