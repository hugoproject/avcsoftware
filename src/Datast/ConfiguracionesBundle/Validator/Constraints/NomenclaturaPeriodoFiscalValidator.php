<?php

namespace Datast\ConfiguracionesBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;

class NomenclaturaPeriodoFiscalValidator extends ConstraintValidator
{
    private $em; 

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

	public function validate($nomenclaturaPeriodoFiscal, Constraint $constraint)
    {

        $qb = $this->em->getRepository('DatastConfiguracionesBundle:NomenclaturaPeriodoFiscal')
            ->createQueryBuilder('npf');

        $cuentaExistente = $qb
            ->where('npf.periodoFiscal = :periodoFiscal')
            ->andWhere('npf.cuenta = :cuenta')
            ->setParameters([
                'periodoFiscal' => $nomenclaturaPeriodoFiscal->getPeriodoFiscal(),
                'cuenta' => $nomenclaturaPeriodoFiscal->getCuenta()
            ])
            ->getQuery()
            ->getResult();

        if($cuentaExistente) {
            $this->context->buildViolation('La cuenta ingresada ya existe')
                ->atPath('cuenta')
                ->addViolation();
        }
    }
}

