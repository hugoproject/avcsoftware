<?php

namespace Datast\ConfiguracionesBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class NomenclaturaPeriodoFiscal extends Constraint
{
    public function validatedBy()
    {
        return 'validator_nomenclatura_periodo_fiscal';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
