<?php

namespace Datast\ConfiguracionesBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class PeriodoFiscal extends Constraint
{
    public function validatedBy()
    {
        return 'validator_periodo_fiscal';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
