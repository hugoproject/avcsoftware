<?php

namespace Datast\ConfiguracionesBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;

class PeriodoFiscalValidator extends ConstraintValidator
{
    private $em; 

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

	public function validate($periodoFiscal, Constraint $constraint)
    {
        if ($periodoFiscal->getFechaInicio() > $periodoFiscal->getFechaFin()) {
            $this->context->buildViolation('La fecha de finalización no debe menor a la fecha de inicio')
                ->atPath('fechaFin')
                ->addViolation();
        }

        if ($periodoFiscal->getFechaInicio() == $periodoFiscal->getFechaFin()) {
            $this->context->buildViolation('Las fechas no deben ser iguales')
                ->atPath('fechaInicio')
                ->addViolation();
        }

        $this->validarPeriodoVigente($periodoFiscal);
    }

    private function validarPeriodoVigente($periodoFiscal)
    {
        $qb = $this->em->getRepository('DatastConfiguracionesBundle:PeriodoFiscal')
            ->createQueryBuilder('pf');

        $periodoFiscalExistente = $qb
            ->where('pf.empresa = :empresa')
            ->andWhere(
                $qb->expr()->andX(
                    $qb->expr()->gte(':fechaInicio', 'pf.fechaInicio'),
                    $qb->expr()->lte(':fechaInicio', 'pf.fechaFin')
                )
            )
            ->setParameters([
                'empresa' => $periodoFiscal->getEmpresa(),
                'fechaInicio' => $periodoFiscal->getFechaInicio()
            ])
            ->getQuery()
            ->getResult();

        if($periodoFiscalExistente) {
            $this->context->buildViolation('Ya existe un periodo definido en el rango de fecha indicado')
                ->atPath('fechaInicio')
                ->addViolation();
        }
    }
}

