<?php

namespace Datast\ConfiguracionesBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LoginEmpresaListener
{
    private $tokenStorage;

    private $autorizationChecker;

    private $router;

    public function __construct(
        $tokenStorage,
        $autorizationChecker,
        Router $router
    )
    {
        $this->tokenStorage = $tokenStorage;
        $this->autorizationChecker = $autorizationChecker;
        $this->router = $router;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (
            // Validar autenticación
            $this->tokenStorage->getToken() &&
            $this->autorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED') &&

            // Y no estamos en la ruta para elegir empresa
            'datast_configuraciones_homepage_index' !== $event->getRequest()->attributes->get('_route', false) &&
            // Y no tiene seteada la empresa
            !$event->getRequest()->getSession()->get('empresa')
        ) {
            $event->setResponse(new RedirectResponse(
                $this->router->generate('datast_configuraciones_homepage_index')
            ));
        }
    }
}
