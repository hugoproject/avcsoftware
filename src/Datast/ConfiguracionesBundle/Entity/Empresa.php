<?php

namespace Datast\ConfiguracionesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Empresas
 *
 * @ORM\Table(name="empresa")
 * @ORM\Entity(repositoryClass="Datast\ConfiguracionesBundle\Repository\EmpresaRepository")
 */
class Empresa
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="razonsocial", type="string", length=250)
     */
    private $razonsocial;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreComercial", type="string", length=250)
     */
    private $nombreComercial;

    /**
     * @var string
     *
     * @ORM\Column(name="nit", type="string", length=64)
     */
    private $nit;

   /**
     * @ORM\ManyToOne(
     *     targetEntity="Moneda"
     * )
     *
     * @ORM\JoinColumn(
     *     name="monedaId", 
     *     nullable=true,
     *     referencedColumnName="id"
     * )
     */
    private $moneda;

   /**
     * @ORM\ManyToOne(
     *     targetEntity="Moneda"
     * )
     *
     * @ORM\JoinColumn(
     *     name="monedaInternacionalId", 
     *     nullable=true,
     *     referencedColumnName="id"
     * )
     */
    private $monedaInternacional;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=250)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=64)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreLogo", type="string", length=250)
     */
    private $nombreLogo;

   /**
     * @ORM\ManyToMany(
     *     targetEntity="Datast\ConfiguracionesBundle\Entity\User",
     *     mappedBy ="empresas"
     * )
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Empresas
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set razonsocial
     *
     * @param string $razonsocial
     *
     * @return Empresas
     */
    public function setRazonsocial($razonsocial)
    {
        $this->razonsocial = $razonsocial;

        return $this;
    }

    /**
     * Get razonsocial
     *
     * @return string
     */
    public function getRazonsocial()
    {
        return $this->razonsocial;
    }

    /**
     * Set nombreComercial
     *
     * @param string $nombreComercial
     *
     * @return Empresas
     */
    public function setNombreComercial($nombreComercial)
    {
        $this->nombreComercial = $nombreComercial;

        return $this;
    }

    /**
     * Get nombreComercial
     *
     * @return string
     */
    public function getNombreComercial()
    {
        return $this->nombreComercial;
    }

    /**
     * Set nit
     *
     * @param string $nit
     *
     * @return Empresas
     */
    public function setNit($nit)
    {
        $this->nit = $nit;

        return $this;
    }

    /**
     * Get nit
     *
     * @return string
     */
    public function getNit()
    {
        return $this->nit;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Empresas
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Empresas
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set nombreLogo
     *
     * @param string $nombreLogo
     *
     * @return Empresas
     */
    public function setNombreLogo($nombreLogo)
    {
        $this->nombreLogo = $nombreLogo;

        return $this;
    }

    /**
     * Get nombreLogo
     *
     * @return string
     */
    public function getNombreLogo()
    {
        return $this->nombreLogo;
    }

    /**
     * Set moneda
     *
     * @param \Datast\ConfiguracionesBundle\Entity\Moneda $moneda
     *
     * @return Empresa
     */
    public function setMoneda(\Datast\ConfiguracionesBundle\Entity\Moneda $moneda = null)
    {
        $this->moneda = $moneda;

        return $this;
    }

    /**
     * Get moneda
     *
     * @return \Datast\ConfiguracionesBundle\Entity\Moneda
     */
    public function getMoneda()
    {
        return $this->moneda;
    }

    /**
     * Add user
     *
     * @param \Datast\ConfiguracionesBundle\Entity\User $user
     *
     * @return Empresa
     */
    public function addUser(\Datast\ConfiguracionesBundle\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Datast\ConfiguracionesBundle\Entity\User $user
     */
    public function removeUser(\Datast\ConfiguracionesBundle\Entity\User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set monedaInternacional
     *
     * @param \Datast\ConfiguracionesBundle\Entity\Moneda $monedaInternacional
     *
     * @return Empresa
     */
    public function setMonedaInternacional(\Datast\ConfiguracionesBundle\Entity\Moneda $monedaInternacional = null)
    {
        $this->monedaInternacional = $monedaInternacional;

        return $this;
    }

    /**
     * Get monedaInternacional
     *
     * @return \Datast\ConfiguracionesBundle\Entity\Moneda
     */
    public function getMonedaInternacional()
    {
        return $this->monedaInternacional;
    }
}
