<?php

namespace Datast\ConfiguracionesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * ConfiguracionNomenclaturaContable
 *
 * @ORM\Table(name="configuracion_nomenclatura_contable")
 * @ORM\Entity(repositoryClass="Datast\ConfiguracionesBundle\Repository\ConfiguracionNomenclaturaContableRepository")
 */
class ConfiguracionNomenclaturaContable
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="activo", type="string", length=8)
     */
    private $activo;

    /**
     * @var string
     *
     * @ORM\Column(name="pasivo", type="string", length=8)
     */
    private $pasivo;

    /**
     * @var string
     *
     * @ORM\Column(name="capital", type="string", length=8)
     */
    private $capital;

    /**
     * @var string
     *
     * @ORM\Column(name="ingreso", type="string", length=8)
     */
    private $ingreso;

    /**
     * @var string
     *
     * @ORM\Column(name="costo", type="string", length=8)
     */
    private $costo;

    /**
     * @var string
     *
     * @ORM\Column(name="gasto", type="string", length=8)
     */
    private $gasto;

    /**
     * @var string
     *
     * @ORM\Column(name="otrosingresos", type="string", length=8)
     */
    private $otrosIngresos;

    /**
     * @var string
     *
     * @ORM\Column(name="otrosegresos", type="string", length=8)
     */
    private $otrosEgresos;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activo
     *
     * @param string $activo
     *
     * @return ConfiguracionNomenclaturaContable
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return string
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set pasivo
     *
     * @param string $pasivo
     *
     * @return ConfiguracionNomenclaturaContable
     */
    public function setPasivo($pasivo)
    {
        $this->pasivo = $pasivo;

        return $this;
    }

    /**
     * Get pasivo
     *
     * @return string
     */
    public function getPasivo()
    {
        return $this->pasivo;
    }

    /**
     * Set capital
     *
     * @param string $capital
     *
     * @return ConfiguracionNomenclaturaContable
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;

        return $this;
    }

    /**
     * Get capital
     *
     * @return string
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * Set ingreso
     *
     * @param string $ingreso
     *
     * @return ConfiguracionNomenclaturaContable
     */
    public function setIngreso($ingreso)
    {
        $this->ingreso = $ingreso;

        return $this;
    }

    /**
     * Get ingreso
     *
     * @return string
     */
    public function getIngreso()
    {
        return $this->ingreso;
    }

    /**
     * Set costo
     *
     * @param string $costo
     *
     * @return ConfiguracionNomenclaturaContable
     */
    public function setCosto($costo)
    {
        $this->costo = $costo;

        return $this;
    }

    /**
     * Get costo
     *
     * @return string
     */
    public function getCosto()
    {
        return $this->costo;
    }

    /**
     * Set gasto
     *
     * @param string $gasto
     *
     * @return ConfiguracionNomenclaturaContable
     */
    public function setGasto($gasto)
    {
        $this->gasto = $gasto;

        return $this;
    }

    /**
     * Get gasto
     *
     * @return string
     */
    public function getGasto()
    {
        return $this->gasto;
    }

    /**
     * Set otrosIngresos
     *
     * @param string $otrosIngresos
     *
     * @return ConfiguracionNomenclaturaContable
     */
    public function setOtrosIngresos($otrosIngresos)
    {
        $this->otrosIngresos = $otrosIngresos;

        return $this;
    }

    /**
     * Get otrosIngresos
     *
     * @return string
     */
    public function getOtrosIngresos()
    {
        return $this->otrosIngresos;
    }

    /**
     * Set otrosEgresos
     *
     * @param string $otrosEgresos
     *
     * @return ConfiguracionNomenclaturaContable
     */
    public function setOtrosEgresos($otrosEgresos)
    {
        $this->otrosEgresos = $otrosEgresos;

        return $this;
    }

    /**
     * Get otrosEgresos
     *
     * @return string
     */
    public function getotrosEgresos()
    {
        return $this->otrosEgresos;
    }
}

