<?php

namespace Datast\ConfiguracionesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * CPO [CLIENTES, PROVEEDORES, OTROS]
 *
 * @ORM\Table(name="cpo")
 * @ORM\Entity(repositoryClass="Datast\ConfiguracionesBundle\Repository\CPORepository")
 */
class CPO
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="razonsocial", type="string", length=250, unique=true)
     */
    private $razonsocial;

    /**
     * @var string
     *
     * @ORM\Column(name="nombreComercial", type="string", length=250)
     */
    private $nombreComercial;

    /**
     * @var string
     *
     * @ORM\Column(name="nit", type="string", length=64, unique=true)
     */
    private $nit;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=254, nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=128, nullable=true)
     */
    private $telefono;

   /**
     * @ORM\ManyToOne(
     *     targetEntity="Pais"
     * )
     *
     * @ORM\JoinColumn(
     *     name="pais_id", 
     *     nullable=false,
     *     referencedColumnName="id"
     * )
     */
    private $pais;

    /**
     * @var string
     *
     * @ORM\Column(name="correoElectronico", type="string", length=250, nullable=true)
     */
    private $correoElectronico;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaDeBaja", type="date", nullable=true)
     */
    private $fechaDeBaja;

   /**
     * @ORM\ManyToOne(
     *     targetEntity="Empresa"
     * )
     *
     * @ORM\JoinColumn(
     *     name="empresaId", 
     *     nullable=true,
     *     referencedColumnName="id"
     * )
     */
    private $empresa;

    /**
     * @var string
     *
     * @ORM\Column(name="anotaciones", type="string", length=255, nullable=true)
     */
    private $anotaciones;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set razonsocial
     *
     * @param string $razonsocial
     *
     * @return CPO
     */
    public function setRazonsocial($razonsocial)
    {
        $this->razonsocial = $razonsocial;

        return $this;
    }

    /**
     * Get razonsocial
     *
     * @return string
     */
    public function getRazonsocial()
    {
        return $this->razonsocial;
    }

    /**
     * Set nombreComercial
     *
     * @param string $nombreComercial
     *
     * @return CPO
     */
    public function setNombreComercial($nombreComercial)
    {
        $this->nombreComercial = $nombreComercial;

        return $this;
    }

    /**
     * Get nombreComercial
     *
     * @return string
     */
    public function getNombreComercial()
    {
        return $this->nombreComercial;
    }

    /**
     * Set nit
     *
     * @param string $nit
     *
     * @return CPO
     */
    public function setNit($nit)
    {
        $this->nit = $nit;

        return $this;
    }

    /**
     * Get nit
     *
     * @return string
     */
    public function getNit()
    {
        return $this->nit;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return CPO
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return CPO
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set pais
     *
     * @param string $pais
     *
     * @return CPO
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return string
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set correoElectronico
     *
     * @param string $correoElectronico
     *
     * @return CPO
     */
    public function setCorreoElectronico($correoElectronico)
    {
        $this->correoElectronico = $correoElectronico;

        return $this;
    }

    /**
     * Get correoElectronico
     *
     * @return string
     */
    public function getCorreoElectronico()
    {
        return $this->correoElectronico;
    }

    /**
     * Set fechaDeBaja
     *
     * @param \DateTime $fechaDeBaja
     *
     * @return CPO
     */
    public function setFechaDeBaja($fechaDeBaja)
    {
        $this->fechaDeBaja = $fechaDeBaja;

        return $this;
    }

    /**
     * Get fechaDeBaja
     *
     * @return \DateTime
     */
    public function getFechaDeBaja()
    {
        return $this->fechaDeBaja;
    }

    /**
     * Set empresa
     *
     * @param string $empresa
     *
     * @return CPO
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return string
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set anotaciones
     *
     * @param string $anotaciones
     *
     * @return CPO
     */
    public function setAnotaciones($anotaciones)
    {
        $this->anotaciones = $anotaciones;

        return $this;
    }

    /**
     * Get anotaciones
     *
     * @return string
     */
    public function getAnotaciones()
    {
        return $this->anotaciones;
    }
}

