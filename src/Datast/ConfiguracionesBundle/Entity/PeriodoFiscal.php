<?php

namespace Datast\ConfiguracionesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * PeriodoFiscal
 *
 * @ORM\Table(name="periodo_fiscal")
 * @ORM\Entity(repositoryClass="Datast\ConfiguracionesBundle\Repository\PeriodoFiscalRepository")
 */
class PeriodoFiscal
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaInicio", type="datetime")
     */
    private $fechaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaFin", type="datetime")
     */
    private $fechaFin;

   /**
     * @ORM\ManyToOne(
     *     targetEntity="Empresa"
     * )
     *
     * @ORM\JoinColumn(
     *     name="empresaId",
     *     nullable=true,
     *     referencedColumnName="id"
     * )
     */
    private $empresa;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     *
     * @return PeriodoFiscal
     */
    public function setFechaInicio(\DateTime $fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     *
     * @return PeriodoFiscal
     */
    public function setFechaFin(\DateTime $fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }


    /**
     * Set empresa
     *
     * @param \Datast\ConfiguracionesBundle\Entity\Empresa $empresa
     *
     * @return PeriodoFiscal
     */
    public function setEmpresa(\Datast\ConfiguracionesBundle\Entity\Empresa $empresa = null)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return \Datast\ConfiguracionesBundle\Entity\Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }
}
