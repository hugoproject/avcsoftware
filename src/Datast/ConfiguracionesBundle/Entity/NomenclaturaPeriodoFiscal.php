<?php

namespace Datast\ConfiguracionesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * NomenclaturaPeriodoFiscal
 * 
 *
 * @ORM\Table(name="nomenclatura_periodo_fiscal")
 * @ORM\Entity(repositoryClass="Datast\ConfiguracionesBundle\Repository\NomenclaturaPeriodoFiscalRepository")
 */
class NomenclaturaPeriodoFiscal
{
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   /**
     * @ORM\ManyToOne(
     *     targetEntity="PeriodoFiscal"
     * )
     *
     * @ORM\JoinColumn(
     *     name="periodo_fiscal_id",
     *     nullable=true,
     *     referencedColumnName="id"
     * )
     */
    private $periodoFiscal;

    /**
     * @var string
     *
     * @ORM\Column(name="cuenta", type="string", length=32)
     */
    private $cuenta;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_cuenta", type="string", length=254)
     */
    private $nombreCuenta;

   /**
     * @ORM\ManyToOne(
     *     targetEntity="NomenclaturaPeriodoFiscal"
     * )
     *
     * @ORM\JoinColumn(
     *     name="nodo_padre_id",
     *     nullable=true,
     *     referencedColumnName="id"
     * )
     */
    private $nodoPadre;

    /**
     * @var string
     *
     * @ORM\Column(name="nivel", type="string", length=255)
     */
    private $nivel;

    /**
     * @var string
     *
     * @ORM\Column(name="altura", type="string", length=255)
     */
    private $altura;

    /**
     * @var string
     *
     * @ORM\Column(name="naturaleza", type="string", length=8)
     */
    private $naturaleza;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_de_cuenta", type="string", length=16)
     */
    private $tipoDeCuenta;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="estado", 
     *     type="boolean",
     *     nullable=true,
     *     options={"default" = "1"}
     * )
     */
    private $estado;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cuenta
     *
     * @param string $cuenta
     *
     * @return NomenclaturaPeriodoFiscal
     */
    public function setCuenta($cuenta)
    {
        $this->cuenta = $cuenta;

        return $this;
    }

    /**
     * Get cuenta
     *
     * @return string
     */
    public function getCuenta()
    {
        return $this->cuenta;
    }

    /**
     * Set nombreCuenta
     *
     * @param string $nombreCuenta
     *
     * @return NomenclaturaPeriodoFiscal
     */
    public function setNombreCuenta($nombreCuenta)
    {
        $this->nombreCuenta = $nombreCuenta;

        return $this;
    }

    /**
     * Get nombreCuenta
     *
     * @return string
     */
    public function getNombreCuenta()
    {
        return $this->nombreCuenta;
    }

    /**
     * Set nodoPadre
     *
     * @param string $nodoPadre
     *
     * @return NomenclaturaPeriodoFiscal
     */
    public function setNodoPadre($nodoPadre)
    {
        $this->nodoPadre = $nodoPadre;

        return $this;
    }

    /**
     * Get nodoPadre
     *
     * @return string
     */
    public function getNodoPadre()
    {
        return $this->nodoPadre;
    }

    /**
     * Set nivel
     *
     * @param string $nivel
     *
     * @return NomenclaturaPeriodoFiscal
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get nivel
     *
     * @return string
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set altura
     *
     * @param string $altura
     *
     * @return NomenclaturaPeriodoFiscal
     */
    public function setAltura($altura)
    {
        $this->altura = $altura;

        return $this;
    }

    /**
     * Get altura
     *
     * @return string
     */
    public function getAltura()
    {
        return $this->altura;
    }

    /**
     * Set naturaleza
     *
     * @param string $naturaleza
     *
     * @return NomenclaturaPeriodoFiscal
     */
    public function setNaturaleza($naturaleza)
    {
        $this->naturaleza = $naturaleza;

        return $this;
    }

    /**
     * Get naturaleza
     *
     * @return string
     */
    public function getNaturaleza()
    {
        return $this->naturaleza;
    }

    /**
     * Set tipoDeCuenta
     *
     * @param string $tipoDeCuenta
     *
     * @return NomenclaturaPeriodoFiscal
     */
    public function setTipoDeCuenta($tipoDeCuenta)
    {
        $this->tipoDeCuenta = $tipoDeCuenta;

        return $this;
    }

    /**
     * Get tipoDeCuenta
     *
     * @return string
     */
    public function getTipoDeCuenta()
    {
        return $this->tipoDeCuenta;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return NomenclaturaPeriodoFiscal
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set periodoFiscal
     *
     * @param \Datast\ConfiguracionesBundle\Entity\PeriodoFiscal $periodoFiscal
     *
     * @return NomenclaturaPeriodoFiscal
     */
    public function setPeriodoFiscal(\Datast\ConfiguracionesBundle\Entity\PeriodoFiscal $periodoFiscal = null)
    {
        $this->periodoFiscal = $periodoFiscal;

        return $this;
    }

    /**
     * Get periodoFiscal
     *
     * @return \Datast\ConfiguracionesBundle\Entity\PeriodoFiscal
     */
    public function getPeriodoFiscal()
    {
        return $this->periodoFiscal;
    }
}
